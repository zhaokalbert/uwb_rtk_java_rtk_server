package datalogger;


public enum Week {
	MONDAY("ζζδΈ?", "Monday", "Mon.", 1),  
    TUESDAY("ζζδΊ?", "Tuesday", "Tues.", 2),  
    WEDNESDAY("ζζδΈ?", "Wednesday", "Wed.", 3),  
    THURSDAY("ζζε?", "Thursday", "Thur.", 4),  
    FRIDAY("ζζδΊ?", "Friday", "Fri.", 5),  
    SATURDAY("ζζε?", "Saturday", "Sat.", 6),  
    SUNDAY("ζζζ?", "Sunday", "Sun.", 7);  
      
    String name_cn;  
    String name_en;  
    String name_enShort;  
    int number;  
      
    Week(String name_cn, String name_en, String name_enShort, int number) {  
        this.name_cn = name_cn;  
        this.name_en = name_en;  
        this.name_enShort = name_enShort;  
        this.number = number;  
    }  
      
    public String getChineseName() {  
        return name_cn;  
    }  
  
    public String getName() {  
        return name_en;  
    }  
  
    public String getShortName() {  
        return name_enShort;  
    }  
  
    public int getNumber() {  
        return number;  
    }  
}
