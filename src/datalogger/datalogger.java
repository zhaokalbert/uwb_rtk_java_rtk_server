package datalogger;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;


public class datalogger {
	private static Date now ;
	private static File file;
	private static FileWriter fileWriter;
    private static BufferedWriter bufferWritter;
    private static boolean IslogfileOpen=false;
    
	public static void openLogfile(){
		
    	now = new Date();
    	long date = now.getTime();
    	String filename =DateUtil.DateToString(new Date(date), DateStyle.YYYY_MM_DD_HH_MM_SS).replaceAll(" ", "").replaceAll("-", "").replaceAll(":", "");
	    file = new File("log/UWB&RTK "+filename+".txt");
	    System.out.println("New file: "+"UWB&RTK "+filename+".txt");
	    if (!file.exists()) {
		    try {
		    	 
				file.createNewFile();
				fileWriter = new FileWriter(file);
				bufferWritter = new BufferedWriter(fileWriter);
				IslogfileOpen=true;
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    


    }
	
	public void closeLogfile(){
    	IslogfileOpen=false;
    	boolean fileclosing=true;
    	while(fileclosing){
	    	try {
	    		if(fileWriter !=null){
				fileWriter.close();
	    		}
	    		if(bufferWritter !=null){
	    		bufferWritter.close();
	    		}
				fileclosing=false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    
    	
    }
	public static void writedata(String str){
        try {
			bufferWritter.write(str);
			bufferWritter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        
	}
	public static void writedataln(String str){
        try {
        	str+="\r\n";
			bufferWritter.write(str);
			bufferWritter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        
	}
}
