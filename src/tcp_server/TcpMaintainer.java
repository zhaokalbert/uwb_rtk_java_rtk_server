package tcp_server;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import rtk_message_forwarder.UartMessageForwarder.UartDataPort;
import uwb_rtk_message_decoder.Utils;

public class TcpMaintainer extends Thread implements UartDataPort{
	private ServerSocket serverSocket;
	private String mIPAdressName;
	private int mPortNumber;
	private Map<String,ReceivingThread> connectionPoll;
	private boolean isRunning = true;
	
	public TcpMaintainer(String IPAdressName, int port){
		mIPAdressName = IPAdressName;
		mPortNumber = port;
		connectionPoll= new HashMap<String,ReceivingThread>();
	}
	
	public void startProcess() {
		try {
			serverSocket = new ServerSocket(mPortNumber, 0, InetAddress.getByName(mIPAdressName ));   
			System.out.println("Wait for any TCP cleint");
			while(isRunning){
				
	            Socket client;
	            
            	client = serverSocket.accept(); 
            	System.out.println("accept client connection"+client.getRemoteSocketAddress().toString());
	            String key = client.getRemoteSocketAddress().toString();
	            
	            ReceivingThread mReceivingThread = new ReceivingThread(client,key);
	            Thread recievedataServerThread = new Thread(mReceivingThread);
	            recievedataServerThread.start();
	            connectionPoll.put(key, mReceivingThread);

	           
			}
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		} 
		for(ReceivingThread thread :connectionPoll.values()) {
			thread.stop();
		}
		System.out.println("TCP thread ends");
		
	}
	
	public void forwardData(byte[] data) {
		for(ReceivingThread thread :connectionPoll.values()) {
			int result = thread.writeData(data);
			if(result ==-1) {
				thread.stop();
				connectionPoll.remove(thread.key);
			}
		}
		
	}
	public void stopThread() {
		
		isRunning =false;
		try {
			serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void run() {
		// TODO Auto-generated method stub
		startProcess();
	}

}
