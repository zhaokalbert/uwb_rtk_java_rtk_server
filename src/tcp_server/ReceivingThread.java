package tcp_server;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

import main.Constants;
import uwb_rtk_message_decoder.SampleMessageReceiver;
import uwb_rtk_message_decoder.UWBRTKDecoder;
import uwb_rtk_message_decoder.Utils;


public class ReceivingThread implements Runnable{
	private Socket client;  
	private int buffersize=10;
	private UWBRTKDecoder mUWBRTKDecoder;
	private SampleMessageReceiver mSampleMessageReceiver;
	public String key;
	private boolean isRunning = true;



    public ReceivingThread(Socket client, String mapkey) {  
        this.client = client;  
        this.key = mapkey;
        this.mUWBRTKDecoder = new UWBRTKDecoder();
        this.mSampleMessageReceiver = new SampleMessageReceiver();
        this.mUWBRTKDecoder.setReceiver(mSampleMessageReceiver);
    }
    
    @Override  
    public void run() {  
    	int num =0;
    	byte[] buf=new byte[5];
    	while(isRunning){
	    	try{
	    		
	        		InputStream in=client.getInputStream();	
	    			num = in.available();
	    			if(num>0) {
	    				buf = in.readNBytes(num);
	    				if(Constants.isRecording) {
	    					String toWrite = System.currentTimeMillis() + " " +key+" "+ Utils.byte2HexStr(buf, buf.length);
	    					datalogger.datalogger.writedataln(toWrite);
	    				}
	    				mUWBRTKDecoder.onDataArriving(buf);
	    			}
	    			Thread.sleep(500);
		
		   }catch(Exception e){
			   e.printStackTrace();
			   stop();
		
		   }

    	}
    	
		try {
			System.out.println(key+"closed");
			client.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
          
    }
    public void stop() {
    	isRunning = false;
    }
    
    public int writeData(byte[] data) {
    	try {
			OutputStream outputStream = client.getOutputStream();
			outputStream.write(data);
			outputStream.flush();
			return 0;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			stop();
		}
    	return -1;
    	
    }
	    
	    
	    
	    
	    

}
