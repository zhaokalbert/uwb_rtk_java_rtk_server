package rtk_message_forwarder;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortMessageListener;

import uwb_rtk_message_decoder.Utils;



public class UartMessageForwarder {
	private SerialPort[] portList;
	private static final int baudrate=115200;
	private SerialPort comPort;
	private UartDataPort mUartDataPort =null;
	
	public UartMessageForwarder(){

	}
	
	public void setDataCallback(UartDataPort forward){
		mUartDataPort = forward;
	}
	
	public SerialPort[] getPortList() {
		portList = SerialPort.getCommPorts();
		return portList;
	}
	
	public void start(SerialPort port) {
		if(comPort !=null) {
			comPort.closePort();
		}
		comPort = port;
		comPort.setBaudRate(baudrate);
		if(comPort.openPort()) {
			System.out.println("UART port open successful");
		}else {
			System.out.println("UART port open failed");
		}
		comPort.addDataListener(new SerialPortMessageListener() {
			 @Override
			   public int getListeningEvents() { return SerialPort.LISTENING_EVENT_DATA_RECEIVED; }

			   @Override
			   public byte[] getMessageDelimiter() { return new byte[] { (byte)0xF7, (byte)0x2A }; }

			   @Override
			   public boolean delimiterIndicatesEndOfMessage() { return true; }

			   @Override
			   public void serialEvent(SerialPortEvent event)
			   {
			      byte[] newData = event.getReceivedData();
	                if(mUartDataPort !=null) {
	                	System.out.println("Sending fixing data");
                        mUartDataPort.forwardData(newData);
                   }
			   }
		});
	}
	
	public void stop() {
		comPort.closePort();
		comPort=null;
	}
	public interface UartDataPort{
		void forwardData(byte[] data);   
	}
	

}
