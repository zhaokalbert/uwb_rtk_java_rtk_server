//package rtk_message_forwarder;
//
//import java.io.BufferedInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.util.TooManyListenersException;
//
//import gnu.io.CommPortIdentifier;
//import gnu.io.PortInUseException;
//import gnu.io.SerialPort;
//import gnu.io.SerialPortEvent;
//import gnu.io.SerialPortEventListener;
//import gnu.io.UnsupportedCommOperationException;
//import tcp_server.TcpMaintainer;
//import uwb_rtk_message_decoder.DecoderInterface.UWBRangingMessage;
//
//public class RtkMessageForwarder implements SerialPortEventListener{
//	private CommPortIdentifier mPortIdentifer=null;
//	private SerialPort serialPort=null;
//	private static final String appName = "UWB&RTK ";
//	private static final int timeout = 2000;//port open timeout
//	private InputStream inputStream;  
//    private OutputStream outputStream;  
//    private static final int baudrate=115200;
//    byte[] readBuffer = new byte[1024]; 
//    private UartDataPort mUartDataPort =null;
//	
//	public RtkMessageForwarder (CommPortIdentifier port, UartDataPort forward){
//		 mUartDataPort = forward;
//		 mPortIdentifer = port;
//	}
//	
//	public void start(){
//		if (serialPort !=null){
//			serialPort.close();
//			serialPort=null;
//		}
//		
//		try{
//			
//            serialPort = (SerialPort)mPortIdentifer.open(appName, timeout);  
//
//        }catch(PortInUseException e){  
//
//            throw new RuntimeException(String.format("failed to open port '%1$s'！",mPortIdentifer.getName()));  
//        }
//		
//		try {                        
//            //Config UART port
//            serialPort.setSerialPortParams(baudrate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);                              
//        } catch (UnsupportedCommOperationException e) {  
//            throw new RuntimeException("configuration failed");
//        }
//
//		try{  
//            inputStream = new BufferedInputStream(serialPort.getInputStream());  
//            
//        }catch(IOException e){  
//
//            throw new RuntimeException("failed to obtain InputStream："+e.getMessage());  
//        }  
//          
//        try{  
//            serialPort.addEventListener(this);  
//        }catch(TooManyListenersException e){ 
//
//            throw new RuntimeException(e.getMessage());  
//        } 
//        
//
//        serialPort.notifyOnDataAvailable(true); 
//
//
//
//	}
//	public void stop() {
//		serialPort.close();
//		serialPort=null;
//	}
//	
//	
//	@Override
//	public synchronized void serialEvent(SerialPortEvent arg0) {
//		// TODO Auto-generated method stub
//		switch(arg0.getEventType()){  
//        case SerialPortEvent.BI:/*Break interrupt,通讯中断*/   
//        case SerialPortEvent.OE:/*Overrun error，溢位错误*/   
//        case SerialPortEvent.FE:/*Framing error，传帧错误*/  
//        case SerialPortEvent.PE:/*Parity error，校验错误*/  
//        case SerialPortEvent.CD:/*Carrier detect，载波检测*/  
//        case SerialPortEvent.CTS:/*Clear to send，清除发送*/   
//        case SerialPortEvent.DSR:/*Data set ready，数据设备就绪*/   
//        case SerialPortEvent.RI:/*Ring indicator，响铃指示*/  
//        case SerialPortEvent.OUTPUT_BUFFER_EMPTY:/*Output buffer is empty，输出缓冲区清空*/   
//            break;  
//        case SerialPortEvent.DATA_AVAILABLE:/*Data available at the serial port，端口有可用数据。读到缓冲数组，输出到终端*/  
//        	
//        	
//        	
//        	
//        	 try {  
//
//                 while (inputStream.available()> 0) { 
//                	 int len =inputStream.available();
//                	 byte[] readBuffer=inputStream.readNBytes(len);  
//                	 if(mUartDataPort !=null) {
//                		 mUartDataPort.forwardData(readBuffer);
//                	 }
//                     
//                 }
//                 
//   
//                 
//             } catch (IOException e) {
//             	
//             } 
//        	 break;
//        	
//        }  
//	}
//	
//	public interface UartDataPort{
//		void forwardData(byte[] data);   
//	}
//}
