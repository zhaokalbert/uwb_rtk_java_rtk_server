package main;
import java.util.Scanner;
import java.util.TooManyListenersException;

import com.fazecast.jSerialComm.SerialPort;


import rtk_message_forwarder.UartMessageForwarder;
import tcp_server.TcpMaintainer;
import uwb_rtk_message_decoder.Utils;

public class main {

	
	public static void main(String[] args) {

		//connect to RTK station via UART
		UartMessageForwarder mPort =new UartMessageForwarder();
		SerialPort[] mPortList =mPort.getPortList();
		System.out.println("Available ports");
		for(int i =0;i<mPortList.length;i++) {
			System.out.println(i+ " " + mPortList[i].getDescriptivePortName());
		}
		System.out.println("Input the index of the RTK ports, by default: 0");

        // Using Scanner for Getting Input from User
        Scanner in = new Scanner(System.in);
        int uartPort ;
        try {
        	uartPort = Integer.valueOf(in.nextLine());
        }catch(Exception e){ 
        	uartPort = 0;
        }
        if(uartPort>=mPortList.length || uartPort<0 ) {
        	uartPort=0;
        }
        System.out.println("Input server ip, by default: "+ Constants.serverIP);
        String serverIP = in.nextLine();
        if(serverIP.length()>0) {
        	
        }else {
        	serverIP = String.valueOf(Constants.serverIP);
        }
        
        System.out.println("Input server port, by default: "+ Constants.port);
        String serverPortString = in.nextLine();
        int serverPort;
        try {
        	serverPort = Integer.valueOf(serverPortString);
        }catch(Exception e){ 
        	serverPort = Constants.port;
        }

        // creat a file if recording is enabled
 		if(Constants.isRecording) {
 			datalogger.datalogger.openLogfile();
 		}
       
		//Start server
        TcpMaintainer mTcpMaintainer =new TcpMaintainer(serverIP, serverPort);
		mTcpMaintainer.start();
		mPort.setDataCallback(mTcpMaintainer);
		if(mPortList.length>0) {
			mPort.start(mPortList[uartPort]);
		}
		
		System.out.println("Press any key to stop...");
		in.nextLine();
		mPort.stop();
		mTcpMaintainer.stopThread();
		// closing scanner
        in.close();
		
	}

}
