package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import uwb_rtk_message_decoder.SampleMessageReceiver;
import uwb_rtk_message_decoder.UWBRTKDecoder;
import uwb_rtk_message_decoder.Utils;

public class OfflineTest {
	private static UWBRTKDecoder mUWBRTKDecoder;
	private static SampleMessageReceiver mSampleMessageReceiver;
	public static void main(String[] args)  {
		
		String fileName = "UWB&RTK 20211209183032.txt";
		
		mUWBRTKDecoder = new UWBRTKDecoder();
        mSampleMessageReceiver = new SampleMessageReceiver();
        mUWBRTKDecoder.setReceiver(mSampleMessageReceiver);
        try {
            BufferedReader in = new BufferedReader(new FileReader("log/"+fileName));
            String str;
            while ((str = in.readLine()) != null) {
                
                String[] s= str.split(" ");
                byte[] data = Utils.hexStringToByteArray(s[2]);
                mUWBRTKDecoder.onDataArriving(data);
                //System.out.println(s[2]);
                TimeUnit.MILLISECONDS.sleep(300);
            }
            
        } catch (Exception  e) {
        	e.printStackTrace();
        }
    }

}
