package uwb_rtk_message_decoder;

public class Utils {
	public static  String byte2HexStr(byte[] b,int nn)
    {
        String stmp="";
        StringBuilder sb = new StringBuilder("");
        for (int n=0;n<nn;n++)
        {
            stmp = Integer.toHexString(b[n] & 0xFF);
            sb.append((stmp.length()==1)? "0"+stmp : stmp);
 //           sb.append(" ");
        }
        return sb.toString().toUpperCase().trim();
    }
	
	public static  String byte2HexStr(byte b)
    {
        String stmp="";
        StringBuilder sb = new StringBuilder("");

            stmp = Integer.toHexString(b & 0xFF);
            sb.append((stmp.length()==1)? "0"+stmp : stmp);

        
        return sb.toString().toUpperCase();
    }
	
	/* s must be an even-length string. */
	public static byte[] hexStringToByteArray(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
	
	
	/* from byte to a int */
	public static int getInt(byte[] arr, int index) {
//		return  (0xff000000     & (arr[index+0] << 24))  |
//				(0x00ff0000     & (arr[index+1] << 16))  |
//				(0x0000ff00     & (arr[index+2] << 8))   |
//				(0x000000ff     &  arr[index+3]);
		return  (0xff000000     & (arr[index+3] << 24))  |   //right endian,  small
				(0x00ff0000     & (arr[index+2] << 16))  |
				(0x0000ff00     & (arr[index+1] << 8))   |
				(0x000000ff     &  arr[index+0]);
	}

	
	public static void printInfo(String info) {
		System.out.println(info);
	}

}
