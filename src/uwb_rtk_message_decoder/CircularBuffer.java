package uwb_rtk_message_decoder;




public class CircularBuffer {
	protected int head;   //for next pull out idx,   oldest data
	protected int tail;   //for new incoming idx,   newest data
	protected int len;    //buf size
	protected byte[] buff;//buf body
	protected int currentSize;
	
	public  CircularBuffer(int l){
		head=0;  
		tail=0;  
		len=l;
		buff=new byte[l];
		currentSize = 0;
	}
	
	/**  
	 * @param num    how many bytes need to be read out
	 * @return  bytes read out
	 */
	public byte[] poll(int num){
		if(num>currentSize) {
			num = currentSize;
		}
		byte[] tr = new byte[num];
		if(head+ num <=len) {
			System.arraycopy(buff, head, tr, 0, num);
			head +=num;
			if(head ==len) {
				head = 0;
			}
		}else {
			int tmp= len-head;
			System.arraycopy(buff, head, tr, 0, tmp);
			System.arraycopy(buff, 0, tr, tmp, num-tmp);
			head = num-tmp;
		}
		currentSize-=num;
		return tr;
	}
	
	
	/**
	 * @param num             number of the bytes to compare
	 * @param offset_buff     must >=0, offset on buff
	 * @param data            data body to compare with the buff
	 * @param offset_data     must >=0, offset on data
	 * @return                -2 or -3 if the buff or the data has not enough bytes
	 * 						  -1 if not match
	 *                        0  if match
	 */
	public int compare( byte[] data, int offset_data,int offset_buff,int num){
		if(num+offset_buff>currentSize) {
			return -2;
		}
		if(num+offset_data>data.length) {
			return -3;
		}
		int tmp_cursor =0;
		int idx_buff = (head+offset_buff)%len;
		int idx_data = offset_data;
		while(tmp_cursor< num) {
			if(buff[idx_buff] == data[idx_data]) {
				idx_buff++;
				idx_data++;
				tmp_cursor++;
				if(idx_buff==len) {
					idx_buff=0;
				}
			}else {
				return  -1;
			}
		}
		return 0;
	}
	
	/**
	 * @param data   data to put in buffer
	 * @return     number of the bytes was put in
	 */
	public int insert(byte[] data) {
		int num = data.length;
		if(num+currentSize> len) {
			num =len -currentSize;
		}
		if(tail + num<=len) {
			System.arraycopy(data,0, buff, tail, num);
			tail+= num;
			if(tail ==len) {
				tail=0;
			}
		}else{
			int tmp= len-tail;
			System.arraycopy(data,0,buff, tail, tmp);
			System.arraycopy(data,tmp, buff, 0, num-tmp);
			tail = num-tmp;
		}
		currentSize += num;
		return num;
	}
	
	public int getCurrentDlen(){
		return currentSize;
	}
	
 
}
