package uwb_rtk_message_decoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UWBRTKDecoder implements DecoderInterface {

	private RtkRawBuffer mRtkRawBuffer;
	private TCPBuffer mTCPBuffer;
	
	private MessageReceiver callbacks = null;
	
	/* Constants for TCP buffer decoding   */
	private final int BUFFER_LEN = 4000;  //circular buffer len
	private final int MIN_FRAME_LEN = 12;  
	private final int MAX_FRAME_LEN = 3000;  //Maximum message len
	private final int ADD_LEN =2;   //ID bytes length 
	private final byte[] UWB_RANGING_HEAD = new byte[]{(byte) 0xff,(byte) 0xff};
	private final byte[] ANCHOR_RTK_HEAD = new byte[]{(byte) 0xff,(byte) 0xdd};
	private final byte[] RTK_RAW_HEAD = new byte[]{(byte) 0xff,(byte) 0xee};
	private final byte[] MESSAGE_TAIL = new byte[]{(byte) 0x0d,(byte) 0x0a};
	/* Constants  for RTK _RAW decoding   */
	private final int MAX_NMEA_LEN = 82+10;
	private final byte[] UBX_HEAD = new byte[]{(byte) 0xb5, (byte) 0x62};
	private final byte[] NMEA_HEAD = new byte[]{(byte) 0x24};  //"$"
	private final byte[] NMEA_TAIL = new byte[]{(byte) 0x0d,(byte) 0x0a};
	
	public UWBRTKDecoder(){
		this.mRtkRawBuffer = new RtkRawBuffer(BUFFER_LEN);
		this.mTCPBuffer  = new TCPBuffer(BUFFER_LEN);
	}

	@Override
	public void onDataArriving(byte[] data) {
		mTCPBuffer.insert(data);
		while(mTCPBuffer.dataSegmentation()!= 0);
		

	}

	@Override
	public void setReceiver(MessageReceiver receiver) {

		this.callbacks = receiver;
	}
	
	
	
	private void processUwbRangingMessage(byte[]data){
		UWBRangingMessage tr=new UWBRangingMessage();
		tr.messageType = MessageType.UWB_RANGING;
		tr.messageBody = new ArrayList<UWBRanging>();
		tr.timestamp = System.currentTimeMillis();
		byte[] tmpID = new byte[2];
		tmpID[0]=data[2];
		tmpID[1]=data[3];
		tr.deviceID = Utils.byte2HexStr(tmpID, tmpID.length);
		tr.MCUtimestamp = (data[7]&0xff)*256*256*256+(data[6]&0xff)*256*256+(data[5]&0xff)*256+(data[4]&0xff);
		int dataLen = data.length;
		int tmpCursor = 10;    //skip head +id + timestamp + seq+ LEN 
		while(tmpCursor<=(dataLen-2-6)) {
			UWBRanging tempItem = new UWBRanging();
			tmpID[0]=data[tmpCursor];
			tmpID[1]=data[tmpCursor+1];
			tempItem.deviceID =Utils.byte2HexStr(tmpID, tmpID.length);
			tempItem.distance = Float.intBitsToFloat(Utils.getInt(data, tmpCursor+2));
			tr.messageBody.add(tempItem);
			tmpCursor+=6;
		}
		callbacks.UWBRangingCallback(tr);
	}
	
	private void processAnchorRtkMessage(byte[]data){
		AnchorRtkMessage tr=new AnchorRtkMessage();
		tr.messageType = MessageType.ANCHOR_RTK;
		tr.messageBody = new ArrayList<UWBRTK>();
		tr.timestamp = System.currentTimeMillis();
		byte[] tmpID = new byte[2];
		tmpID[0]=data[2];
		tmpID[1]=data[3];
		tr.deviceID = Utils.byte2HexStr(tmpID, tmpID.length);
		
		int dataLen = data.length;
		int tmpCursor = 6;  //skip head +id +LEN 
		boolean isAdded = false;
		int skipCounter =0;
		while(tmpCursor<dataLen-2) {
			isAdded =false;
			UWBRTK tempItem = new UWBRTK();
			tmpID[0]=data[tmpCursor];
			tmpID[1]=data[tmpCursor+1];
			tempItem.deviceID =Utils.byte2HexStr(tmpID, tmpID.length);
			int nmeaCounter =0;
			while(nmeaCounter <MAX_NMEA_LEN && nmeaCounter+tmpCursor+ADD_LEN<(dataLen-2)) {
				if(data[nmeaCounter+tmpCursor+ADD_LEN] == NMEA_TAIL[0] && data[nmeaCounter+tmpCursor+ADD_LEN+1] == NMEA_TAIL[1]) {
					byte[] tmpBytes =new byte[nmeaCounter+2];
					System.arraycopy(data,tmpCursor+ADD_LEN, tmpBytes, 0, nmeaCounter+2);
					tempItem.rtkMessage = new String(tmpBytes);
					tmpCursor+=(nmeaCounter+ADD_LEN+2);
					tr.messageBody.add(tempItem);
					isAdded = true;
					if(skipCounter>0) {
						Utils.printInfo("Anchor rtk data skipped "+skipCounter+ " bytes");
						skipCounter =0;
					}
					break;
				}else {
					nmeaCounter++;
				}
			}
			if(isAdded) {
				
			}else {
				tmpCursor++;
				skipCounter++;
			}
			
		}
		
		if(tr.messageBody.size()>0) {
			callbacks.AnchorRTKCallback(tr);
		}

	}

	private void processRtkRawMessage(byte[]data){
		int len = data.length;
		if(len>=MIN_FRAME_LEN) {
			byte[] tmp = new byte[len-MIN_FRAME_LEN];
			System.arraycopy(data,10, tmp, 0, len-MIN_FRAME_LEN);  //skip head +id + timestamp +  LEN 
			mRtkRawBuffer.insert(tmp);
			byte[] tmpID = new byte[2];
			tmpID[0]=data[2];
			tmpID[1]=data[3];
			mRtkRawBuffer.setDeviceID(tmpID);
			RTKRawMessage mRTKRawMessage =mRtkRawBuffer.dataSegmentation();
			if(mRTKRawMessage.UBXMessageBody.size() >0 ||mRTKRawMessage.NMEAMessageBody.size() >0 ) {
				mRTKRawMessage.MCUtimestamp = (data[7]&0xff)*256*256*256+(data[6]&0xff)*256*256+(data[5]&0xff)*256+(data[4]&0xff);
				callbacks.RTKRawCallback(mRTKRawMessage);
			}
		}
		
	}
	
	private int checUbxFrame(byte[]data){
		return 1;
		
	}

	
	private class RtkRawBuffer extends CircularBuffer{
		private int skipCounter =0;
		private String recentID ="";
		private final int MAX_UBX_LEN = 3000;
		private final int MIN_UBX_LEN = 8;
		
		
		public RtkRawBuffer(int l) {
			super(l);
			// TODO Auto-generated constructor stub
		}
		public void setDeviceID(byte[] id) {
			recentID =Utils.byte2HexStr(id, id.length);
		}
		
		public RTKRawMessage dataSegmentation() {
			RTKRawMessage tr=new RTKRawMessage();
			tr.messageType = MessageType.RTK_RAW;
			tr.UBXMessageBody = new ArrayList<byte[]>();
			tr.NMEAMessageBody = new ArrayList<String>();
			tr.timestamp = System.currentTimeMillis();
			tr.deviceID = new String(recentID);
			while(currentSize >= MIN_UBX_LEN) {
				if(compare(UBX_HEAD,0,0,2) ==0) {
					int idxMessagelen = head+4;
					int messageLen = ((buff[(idxMessagelen+1)%len] & 0xff) << 8) | (buff[(idxMessagelen)%len] & 0xff)+ MIN_UBX_LEN;
					if(messageLen <=currentSize) {
						if(skipCounter >0) {
							Utils.printInfo("rtk raw data skipped "+skipCounter+ " bytes");
							skipCounter=0;
						}
						byte[] data = poll(messageLen);
						if(checUbxFrame(data)==1) {
							tr.UBXMessageBody.add(data);
						}

					}else {
						return tr;  //buffer has no enough bytes
					}
				}else if(compare(NMEA_HEAD,0,0,1) ==0) {
					int tmpCursor =0;
					int isEndMatching = compare(NMEA_TAIL,0,tmpCursor,2);
					while(isEndMatching == -1 &&tmpCursor < MAX_NMEA_LEN) { //while not match
						tmpCursor++;
						isEndMatching = compare(NMEA_TAIL,0,tmpCursor,2);
					}
					if(isEndMatching==0) {                             //find a complete NMEA message
						if(skipCounter >0) {
							Utils.printInfo("rtk raw data skipped "+skipCounter+ " bytes");
							skipCounter=0;
						}
						byte[] data = poll(tmpCursor+2);
						tr.NMEAMessageBody.add(new String(data));
					}else if(isEndMatching==-2) {                      //buffer has no enough bytes
						return tr;  
					}else if(tmpCursor >= MAX_NMEA_LEN) {             //too long, must be a wrong NMEA head
						poll(1);//skip one byte
						skipCounter++;
					}else {                                            //shall not be here, just in case
						poll(1);//skip one byte
						skipCounter++;
					}
				}else {
					poll(1);//skip one byte
					skipCounter++;
				}
			}
			return tr;
		}
		
	}
	
	private class TCPBuffer extends CircularBuffer{

		private int skipCounter =0;

		
		public TCPBuffer(int l) {
			super(l);
			// TODO Auto-generated constructor stub
		}
		
		public int dataSegmentation() {
			while(currentSize >= MIN_FRAME_LEN) {
				if(compare(UWB_RANGING_HEAD,0,0,2) ==0) {


					int idxMessagelen = head+9;
					int messageLen = buff[(idxMessagelen)%len]*6 + MIN_FRAME_LEN;
					if(currentSize>= messageLen &&  compare(MESSAGE_TAIL,0,messageLen-2,2) ==0 ) {  //buffer has the complete frame  and end with an MESSAGE TAIL
						byte[] data = poll(messageLen);
						if(skipCounter >0) {
							Utils.printInfo("skipped "+skipCounter+ " bytes");
							skipCounter=0;
						}
						processUwbRangingMessage(data);
						return 1;
					}else if(currentSize< messageLen ){    
						if(messageLen> MAX_FRAME_LEN) {    //Frame length is too long, the frame head may be wrong
							poll(1);//skip one byte
							skipCounter++;
						}else {
							return 0;  //buffer has no enough data
						}
					}else { // MESSAGE TAIL is not matched
						poll(1);//skip one byte
						skipCounter++;
					}
				}else if(compare(ANCHOR_RTK_HEAD,0,0,2) ==0) {


					int idxMessagelen = head+4;
					int messageLen = ((buff[(idxMessagelen+1)%len] & 0xff) << 8) | (buff[(idxMessagelen)%len] & 0xff)+ MIN_FRAME_LEN-4;  
					if(currentSize>= messageLen &&  compare(MESSAGE_TAIL,0,messageLen-2,2) ==0 ) {  //buffer has the complete frame  and end with an MESSAGE TAIL
						byte[] data = poll(messageLen);
						if(skipCounter >0) {
							Utils.printInfo("skipped "+skipCounter+ " bytes");
							skipCounter=0;
						}
						processAnchorRtkMessage(data);
						return 1;
					}else if(currentSize< messageLen ){    
						if(messageLen> MAX_FRAME_LEN) {    //Frame length is too long, the frame head may be wrong
							poll(1);//skip one byte
							skipCounter++;
						}else {
							return 0;  //buffer has no enough data
						}
					}else { // MESSAGE TAIL is not matched
						poll(1);//skip one byte
						skipCounter++;
					}
				}else if(compare(RTK_RAW_HEAD,0,0,2) ==0) {


					int idxMessagelen = head+8;
					int messageLen = ((buff[(idxMessagelen+1)%len] & 0xff) << 8) | (buff[(idxMessagelen)%len] & 0xff)+ MIN_FRAME_LEN;
					if(currentSize>= messageLen &&  compare(MESSAGE_TAIL,0,messageLen-2,2) ==0 ) {  //buffer has the complete frame  and end with an MESSAGE TAIL
						byte[] data = poll(messageLen);
						if(skipCounter >0) {
							Utils.printInfo("skipped "+skipCounter+ " bytes");
							skipCounter=0;
						}
						processRtkRawMessage(data);
						return 1;
					}else if(currentSize< messageLen ){    
						if(messageLen> MAX_FRAME_LEN) {    //Frame length is too long, the frame head may be wrong
							poll(1);//skip one byte
							skipCounter++;
						}else {
							return 0;  //buffer has no enough data
						}
					}else { // MESSAGE TAIL is not matched
						poll(1);//skip one byte
						skipCounter++;
					}
				}else {
					
					poll(1);//skip one byte
					skipCounter++;
				}
			}
			return 0;
		}
		
	}

}

