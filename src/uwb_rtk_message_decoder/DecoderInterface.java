package uwb_rtk_message_decoder;

import java.util.List;

/**
 * 	每一个TCP client需要有各自的 DecoderInterface实例
 */
public interface DecoderInterface {
	
	/**
     * 	每次接到同一个TCP client的data，通过该方法输入SDK
     */
	public void onDataArriving(byte[] data);
	
	/**
     * 	设置解码后消息的接收实例，该接收实例必须继承MessageReceiver接口
     */
	public void setReceiver(MessageReceiver receiver);
	
	
	/**
     * 	定义解码后的message结构的父类,所有解码后的消息结构继承该类，各子类仅仅是消息的body不同
     */
    public class MessageDecoded{

        public String deviceID;         //设备ID， 需全局唯一。  通常可用两比特ID转化字符串，比如“OAO1”
        public long timestamp;          //接收时间戳  以解码出完整数据的时间为准
        public MessageType messageType;  //消息类型 
    }
    
    /**
     * 	定义解码后的uwb ranging 消息结构，除了MessageDecoded所包含的信息外，还有一个list包含各anchor的uwb测距结果
     */
    public class UWBRangingMessage extends MessageDecoded {
        public List<UWBRanging> messageBody; 
        public long MCUtimestamp;
    }
    
    /**
     * 	定义解码后的anchor rtk 消息结构，除了MessageDecoded所包含的信息外，还有一个list包含各anchor的GNGGA消息
     */
    public class AnchorRtkMessage extends MessageDecoded {
        public List<UWBRTK> messageBody; 
    }
    /**
     * 	定义解码后的uwb rtk 消息结构，除了MessageDecoded所包含的信息外，还有两个list，各自包含UBX和NMEA的消息
     */
    public class RTKRawMessage extends MessageDecoded {
        public List<byte[]> UBXMessageBody;      
        public List<String> NMEAMessageBody; 
        public long MCUtimestamp;
    }
    
    
    /**
     * 	开发者接收消息的类，请继承该接口，以获得所需的message
     */
    public interface MessageReceiver {
    	void UWBRangingCallback(UWBRangingMessage message);   
    	void AnchorRTKCallback(AnchorRtkMessage message);
    	void RTKRawCallback(RTKRawMessage message);
    	void ErrorCallback(ErrorType error);
    }
    
    
    /**
     * 	message消息类型
     */
    enum MessageType {
        UWB_RANGING,
        ANCHOR_RTK,
        RTK_RAW,
        NONE,
      }
    
    
    /**
     * 	error类型
     */
    enum ErrorType {
        BROKEN_MESSAGE,  //包头包尾验证失败
        BROKEN_UWB_RANGING_MESSAGE,  //帧结构验证失败
        BROKEN_ANCHOR_RTK_MESSAGE,  //帧结构验证失败
        BROKEN_RTK_RAW_MESSAGE,  //帧结构验证失败
        
      }
    
    /**
     * 	
     */
    public class UWBRanging{
    	public String deviceID;    //测距anchor方的ID
    	public float distance;    //测距结果
    }

    
    /**
     * 	
     */
    public class UWBRTK{
    	public String deviceID;    //测距anchor方的ID
    	public String rtkMessage;  //默认是NMEA的GNGGA消息
    }

}
