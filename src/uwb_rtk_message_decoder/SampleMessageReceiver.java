package uwb_rtk_message_decoder;

import uwb_rtk_message_decoder.DecoderInterface.AnchorRtkMessage;
import uwb_rtk_message_decoder.DecoderInterface.ErrorType;
import uwb_rtk_message_decoder.DecoderInterface.MessageReceiver;
import uwb_rtk_message_decoder.DecoderInterface.RTKRawMessage;
import uwb_rtk_message_decoder.DecoderInterface.UWBRangingMessage;

public class SampleMessageReceiver implements MessageReceiver{
	private boolean isPrintingDetail =true;

	@Override
	public void UWBRangingCallback(UWBRangingMessage message) {
		// TODO Auto-generated method stub
		Utils.printInfo("uwb ranging message received");
		if(isPrintingDetail) {
			String print ="UWB Ranging Message, Device ID:"+ message.deviceID +" Ranging num:"+message.messageBody.size() +" MCU time "+message.MCUtimestamp +"\r\n";
			for(int i =0;i<message.messageBody.size();i++) {
				print+=(message.messageBody.get(i).deviceID +" "+ String.valueOf(message.messageBody.get(i).distance) +"\r\n");
			}
			Utils.printInfo(print);
		}
	}

	@Override
	public void AnchorRTKCallback(AnchorRtkMessage message) {
		// TODO Auto-generated method stub
		Utils.printInfo("anchor rtk message received");
		if(isPrintingDetail) {
			String print ="Anchor RTK Message, Device ID:"+ message.deviceID +" Rtk num:"+message.messageBody.size() +"\r\n";
			for(int i =0;i<message.messageBody.size();i++) {
				print+=(message.messageBody.get(i).deviceID +" "+ message.messageBody.get(i).rtkMessage +"\r\n");
			}
			Utils.printInfo(print);
		}
	}

	@Override
	public void RTKRawCallback(RTKRawMessage message) {
		// TODO Auto-generated method stub
		Utils.printInfo("rtk raw message received");
		if(isPrintingDetail) {
			String print ="RTK Raw Message, Device ID:"+ message.deviceID +" MCU time "+message.MCUtimestamp +"\r\n";
			for(int i =0;i<message.NMEAMessageBody.size();i++) {
				print+=message.NMEAMessageBody.get(i) ;      //print NMEA message
			}
			for(int i =0;i<message.UBXMessageBody.size();i++) {
				print+=(Utils.byte2HexStr(message.UBXMessageBody.get(i),4 )+"\r\n");  //print UBX message head
			}
			Utils.printInfo(print);
		}
	}

	@Override
	public void ErrorCallback(ErrorType error) {
		// TODO Auto-generated method stub
		Utils.printInfo("error "+ error.name());
	}

}
